// ## Elimina el contenido del section de id="contenido" ## //
$("#limpiar").click(function(){
  reiniciar(false);
});


$("#ejecutar").click(function(){

  Estado("Pidiendo el menu...");
  
  GetJsonCoffees().then(function(menu){
    reiniciar(true);
    Estado("Listando menu...");
    setTimeout(function(){
      menu.forEach(element => {
        $("#images-coffe").append('<div class="block-coffe" data-name="'+element.nombre+'"> <img src="Img/coffee2.png" alt="Cafe" class="cafe-icon" width="150px" height="150px"> <center><label> '+element.nombre+' : $'+element.precio+' </label></center>    </div>');        
      });
      Estado("Menu listado!");
    } , 1000);

  });

});

function reiniciar(flag){
  if(flag){
    $('#images-coffe').html("");    
  }
  $('#contenido').html("");
  $('#imagenes').html("");
}


async function GetJsonCoffees(){
  await Esperar(1000);
  Estado("Trayendo el menu..");
  let respuesta = await fetch("data/menu.json");
  let menu = await respuesta.json();
  await Esperar(2000);
  return menu;
}

function Preparando(texto){
  setTimeout(function(){
    DisplayMessenger(texto);
  }, 800);
}

function CalentarAgua(){
  return new Promise(function(resolve, reject){
    Preparando(" > Preparando agua...");
    setTimeout(function(){
        resolve("Agua preparada");
        reject("No hay agua para el cafe");
    }, 1800);
  });
}

function MolerCafe(){
  return new Promise(function(resolve, reject){
    Preparando(" > Moliendo cafe...");
    setTimeout(function(){
        resolve("Cafe Molido");
        reject("No hay granos de cafe");
    }, 1800);
  });
}
function PrepararTaza(){
  return new Promise(function(resolve, reject){
    Preparando(" > Preparando taza...");
    setTimeout(function(){
        resolve("Taza preparada");
        reject("No hay tazas para servir cafe");
    }, 1800);
  });
}

function AgregarAzucar(){
  return new Promise(function(resolve, reject){
    Preparando(" > Agregando azucar...");
    setTimeout(function(){
        resolve("Azucar agregada");
        reject("No hay azucar para el cafe");
    }, 1800);
  });
}

function ServirCafe(){
  return new Promise(function(resolve, reject){
    Preparando(" > Sirviendo cafe...");
    setTimeout(function(){
        resolve("Cafe servido!");
        reject("La maquina se encuentra fuera de servicio");
    }, 1800);
  });
}

function DisplayMessenger(datos){
  $("#contenido").append("- " + datos);
  $("#contenido").append("<br>");
}

$(document).on('click', '.block-coffe', function(){
  let coffee = $(this).attr("data-name");
  Estado("Proceso de preparacion de "+coffee+"!");

  CalentarAgua()
    .then(function(datos){
        DisplayMessenger(datos)
        return MolerCafe();
    })
    .then(function(datos){
      DisplayMessenger(datos)
        return PrepararTaza();
    })
    .then(function(datos){
      DisplayMessenger(datos)
        return AgregarAzucar();
    }).then(function(datos){
      DisplayMessenger(datos)
        return ServirCafe();
    })
    .then(function(datos){
      DisplayMessenger(datos)
    })
    .then(function(datos){
      $("#imagenes").append('<img src="Img/ok.png" alt="Cafe echo" height="150" width="150">');
      Estado("Cafe preparado exitosamente!");
    })
    .catch(function(error){
        console.error("Error: " + error);
    })
});


// ## FUNCIONES COMPLEMETARIAS ## //

function GetList(menu){
  var items = [];

  for (var item in menu) {
    items.push("<li data-coffe-type='"+item+"'>" + item + " : " + menu[item] + "</li>" );
  }

  let list = $("<ul/>");
  list.addClass('lista').html(items.join(''));

  return list;
}

async function Esperar(tiempo){
  return new Promise((resolve, reject) => setTimeout(resolve, tiempo));
}

function Estado(texto){
  $("#estado").text(texto);
}

